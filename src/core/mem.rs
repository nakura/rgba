use core::marker::PhantomData;

#[derive(Debug)]
pub struct R;
#[derive(Debug)]
pub struct W;
#[derive(Debug)]
pub struct RW;
pub trait TypeWidth {}

impl TypeWidth for u8 {}
impl TypeWidth for u16 {}
impl TypeWidth for u32 {}

#[derive(Debug)]
pub struct Addr<T, Access> {
    addr: usize,
    _type: PhantomData<T>,
    _access: PhantomData<Access>,
}

impl<T, Access> Addr<T, Access>
where
    T: TypeWidth,
{
    pub const fn new(addr: usize) -> Self {
        Self {
            addr,
            _type: PhantomData::<T>,
            _access: PhantomData::<Access>,
        }
    }

    pub const fn cast<U: TypeWidth>(self) -> Addr<U, Access> {
        Addr {
            addr: self.addr,
            _type: PhantomData::<U>,
            _access: PhantomData::<Access>,
        }
    }
}

impl<T, Access> core::clone::Clone for Addr<T, Access>
where
    T: TypeWidth,
{
    fn clone(&self) -> Self {
        Self::new(self.addr)
    }
}

impl<T, Access> core::marker::Copy for Addr<T, Access> where T: TypeWidth {}

impl<T> Addr<T, R> {
    pub fn read(&self) -> T {
        unsafe { (self.addr as *const T).read_volatile() }
    }
}

impl<T> Addr<T, W> {
    pub fn write(&self, val: T) {
        unsafe { (self.addr as *mut T).write_volatile(val) }
    }
}

impl<T> Addr<T, RW>
//where
//    T: core::ops::BitOr<Output = T>,
{
    pub fn read(&self) -> T {
        unsafe { (self.addr as *const T).read_volatile() }
    }

    pub fn write(&self, val: T) {
        unsafe { (self.addr as *mut T).write_volatile(val) }
    }
}

#[derive(Debug)]
pub struct AddrBlk<T, Access, const L: usize> {
    blk_start: Addr<T, Access>,
}

impl<T, Access, const L: usize> AddrBlk<T, Access, L>
where
    T: TypeWidth,
{
    pub const fn new(blk_start: usize) -> Self {
        Self {
            blk_start: Addr::new(blk_start),
        }
    }

    pub const fn cast<U: TypeWidth>(self) -> AddrBlk<U, Access, L> {
        AddrBlk {
            blk_start: Addr {
                addr: self.blk_start.addr,
                _type: PhantomData::<U>,
                _access: PhantomData::<Access>,
            },
        }
    }

    pub const fn len(&self) -> usize {
        L
    }

    pub const fn index(&self, index: usize) -> Addr<T, Access>
    where
        T: TypeWidth,
    {
        assert!(index < L);

        Addr::new(self.blk_start.addr + (index * core::mem::size_of::<T>()))
    }

    pub fn iter(&self) -> AddrBlkIter<T, Access, L> {
        self.into_iter()
    }
}

impl<T, Access, const L: usize> core::clone::Clone for AddrBlk<T, Access, L>
where
    T: TypeWidth,
{
    fn clone(&self) -> Self {
        Self::new(self.blk_start.addr)
    }
}

impl<T, Access, const L: usize> core::marker::Copy for AddrBlk<T, Access, L> where T: TypeWidth {}

pub struct AddrBlkIter<T, Access, const L: usize> {
    start: AddrBlk<T, Access, L>,
    count: usize,
}

impl<T, Access, const L: usize> Iterator for AddrBlkIter<T, Access, L>
where
    T: TypeWidth,
{
    type Item = Addr<T, Access>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.count > L {
            None
        } else {
            let r = Some(self.start.index(self.count));
            self.count += 1;
            r
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.count, Some(L))
    }

    fn count(self) -> usize {
        L - self.count
    }

    fn last(self) -> Option<Self::Item> {
        Some(self.start.index(L - 1))
    }
}

impl<T, Access, const L: usize> IntoIterator for AddrBlk<T, Access, L>
where
    T: TypeWidth,
{
    type Item = Addr<T, Access>;
    type IntoIter = AddrBlkIter<T, Access, L>;

    fn into_iter(self) -> Self::IntoIter {
        AddrBlkIter {
            start: self,
            count: 0,
        }
    }
}