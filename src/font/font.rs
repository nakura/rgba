use crate::graphics::mem::{PalRam, VramMode3};
pub static FONT: &'static [u8] = include_bytes!("../../font/font.bin");
use byteorder::{ByteOrder, LittleEndian};

pub fn setup_font() {
    let c = FONT.len() / 4;
    let mut vram_iter = VramMode3::new::<u32>().iter().skip(0x100);

    for i in 0..c {
        vram_iter
            .next()
            .unwrap()
            .write(LittleEndian::read_u32(&FONT[i * 4..(i * 4) + 4]))
    }

    let font_pal: [u16; 16] = [
        0x4180, 0x7FFF, 0x318C, 0x0000, 0x4180, 0x3960, 0x3140, 0x2920, 0x49A0, 0x5C10, 0x59E0,
        0x294A, 0x4294, 0x001F, 0x03FF, 0x7C00,
    ];

    for (i, w) in font_pal.iter().enumerate() {
        PalRam::new().bg.index(i).write(*w)
    }
}
