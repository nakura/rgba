#[macro_export]
macro_rules! impl_bitfield_to_prim {
    ($target:tt => u16) => {
		impl $target {
			pub fn to_u16le(self) -> u16 {
				byteorder::LittleEndian::read_u16(&self.into_bytes())
			}
		}
	}
}

#[macro_export]
macro_rules! impl_reg_deref {
    ($reg:tt, $regblk:tt) => {
		impl core::ops::Deref for $reg {
			type Target = $regblk;

			fn deref(&self) -> &Self::Target {
				unsafe { &*(self.0 as *const $regblk) }
			}
		}
	}
}