use super::common::*;
use super::mem::*;
use super::regs::*;
use crate::impl_bitfield_to_prim;
use modular_bitfield::prelude::*;
use byteorder::{ByteOrder};
use core::fmt::{Error, Write};

pub struct Lcd {
    pub regs: LcdRegs,
}

pub const LCD: Lcd = Lcd {
    regs: LcdRegs { 0: DISPCNT_ADDR },
};

#[bitfield(bytes = 2)]
pub struct BgMapTextEntry {
    pub tile_num: B10,
    pub h_flip: B1,
    pub v_flop: B1,
    pub pal_num: B4,
}

impl_bitfield_to_prim!(BgMapTextEntry => u16);

pub struct ScreenWriter {
    column_position: usize,
    row_position: usize,
    bg_map: ScreenAddrBlock<u16>,
}

impl Write for ScreenWriter {
    fn write_char(&mut self, c: char) -> Result<(), Error> {
        self.bg_map
            .index(self.column_position + (self.row_position * (SCREEN_TILE_WIDTH + 2)))
            .write(BgMapTextEntry::new().with_tile_num(c as u16).to_u16le());

        Ok(())
    }

    fn write_str(&mut self, s: &str) -> Result<(), Error> {
        for chr in s.bytes() {
            match chr {
                0x20..=0x7E => self.write_char(chr as char).unwrap(),
                b'\n' => {
                    self.new_line();
                    continue;
                }
                _ => self.write_char(0xFE as char).unwrap(),
            }

            self.column_position += 1;

            if self.column_position == (SCREEN_TILE_WIDTH) {
                self.new_line()
            }
        }

        Ok(())
    }
}

impl ScreenWriter {
    pub fn new(bg_map: ScreenAddrBlock<u16>) -> Self {
        Self {
            column_position: 0,
            row_position: 0,
            bg_map,
        }
    }

    pub fn new_line(&mut self) {
        self.row_position += 1;
        self.column_position = 0;
    }
}
