use crate::impl_bitfield_to_prim;
use modular_bitfield::prelude::*;
use byteorder::{ByteOrder};

#[bitfield(bytes = 2)]
pub struct Color15 {
    pub r: B5,
    pub g: B5,
    pub b: B5,
    #[skip]
    pub unused: B1,
}

impl_bitfield_to_prim!(Color15 => u16);