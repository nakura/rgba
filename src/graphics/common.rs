pub const VRAM_START: usize = 0x6000_000;
pub const VRAM_END: usize = VRAM_START + VRAM_TOTAL_SIZE;
pub const PAL_RAM_START: usize = 0x5000_000;
pub const BG_PAL_RAM_START: usize = 0x5000_000;
pub const OBJ_PAL_RAM_START: usize = 0x5000_200;


// Sizes
pub const VRAM_TOTAL_SIZE: usize = 0x18000;
pub const VRAM_MODE3_SIZE: usize = 0x12C00;
pub const CHAR_BLOCK_SIZE: usize = 0x4000;
pub const SCREEN_BLOCK_SIZE: usize = 0x800;
pub const PAL_RAM_SIZE: usize = 0x400;
pub const BG_PAL_RAM_SIZE: usize = 0x200;
pub const OBJ_PAL_RAM_SIZE: usize = 0x200;

// Reg blk start
pub const DISPCNT_ADDR: usize = 0x4000_000;

// Screen
pub const SCREEN_WIDTH: usize = 240;
pub const SCREEN_HEIGHT: usize = 160;

pub const SCREEN_TILE_WIDTH: usize = SCREEN_WIDTH / 8;
pub const SCREEN_TILE_HEIGHT: usize = SCREEN_WIDTH / 8;