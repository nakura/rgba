use crate::impl_reg_deref;
use tock_registers::registers::{ReadOnly, ReadWrite};
use tock_registers::{register_bitfields, register_structs};

// TODO: Seems like tock_registers .val() func for Field
// does not perform compile time check on val, only that it must
// be within max size of T. Look into modifying the lib....


///  LCD I/O Display Control
pub struct LcdRegs(pub(crate) usize);
impl_reg_deref!(LcdRegs, LcdRegBlk);

register_structs! {
    pub LcdRegBlk {
        (0x000 => pub dispcnt: ReadWrite<u16, DispCnt::Register>),
        (0x002 => pub greenswap: ReadWrite<u16, GreenSwap::Register>),
        (0x004 => pub dispstat: ReadWrite<u16, DispStat::Register>),
        (0x006 => pub vcount: ReadOnly<u16, VCount::Register>),
        (0x008 => pub bg0cnt: ReadWrite<u16, BGXCnt::Register>),
        (0x00A => pub bg1cnt: ReadWrite<u16, BGXCnt::Register>),
        (0x00C => pub bg2cnt: ReadWrite<u16, BGXCnt::Register>),
        (0x00E => pub bg3cnt: ReadWrite<u16, BGXCnt::Register>),
        (0x010 => @END),
    }
}

// 4000000h - DISPCNT - LCD Control (Read/Write)
register_bitfields! {
    u16,
    pub DispCnt [
    BgMode OFFSET(0) NUMBITS(3) [
        Mode0 = 0,
        Mode1 = 1,
        Mode2 = 2,
        Mode3 = 3,
        Mode4 = 4,
        Mode5 = 5
    ],
    Unused OFFSET(3) NUMBITS(1) [],
    DisplayFrame OFFSET(4) NUMBITS(1) [] ,
    HBlankFree OFFSET(5) NUMBITS(1) [],
    ObjCharVramMap OFFSET(6) NUMBITS(1) [],
    ForcedBlank OFFSET(7) NUMBITS(1) [],
    DisplayBG0 OFFSET(8) NUMBITS(1) [],
    DisplayBG1 OFFSET(9) NUMBITS(1) [],
    DisplayBG2 OFFSET(10) NUMBITS(1) [],
    DisplayBG3 OFFSET(11) NUMBITS(1) [],
    DisplayObj OFFSET(12) NUMBITS(1) [],
    DisplayWin0 OFFSET(13) NUMBITS(1) [],
    DisplayWin1 OFFSET(14) NUMBITS(1) [],
    DisplayObjWin OFFSET(15) NUMBITS(1) [],
    ]
}

// 4000002h - Undocumented - Green Swap (R/W)
register_bitfields! {
    u16,
    pub GreenSwap [
        GreenSwap OFFSET(0) NUMBITS(1),
        Unused OFFSET(1) NUMBITS(15),
    ]
}

//  4000004h - DISPSTAT - General LCD Status (Read/Write)
register_bitfields! {
    u16,
    pub DispStat [
        VBlank OFFSET(0) NUMBITS(1),
        HBlank OFFSET(1) NUMBITS(1),
        VCounter OFFSET(2) NUMBITS(1),
        VBlankIrqEnable OFFSET(3) NUMBITS(1),
        HBlankIrqEnable OFFSET(4) NUMBITS(1),
        VCounterIrqEnable OFFSET(5) NUMBITS(1),
        Unused OFFSET(5) NUMBITS(2),
        VCountSetting OFFSET(7) NUMBITS(8),
    ]
}

//  4000006h - VCOUNT - Vertical Counter (Read only)
register_bitfields! {
    u16,
    pub VCount [
        CurrentScanline OFFSET(0) NUMBITS(8),
        Unused OFFSET(8) NUMBITS(8),
    ]
}

// 4000008h - BG0CNT - BG0 Control (R/W) (BG Modes 0,1 only)
// 400000Ah - BG1CNT - BG1 Control (R/W) (BG Modes 0,1 only)
// 400000Ch - BG2CNT - BG2 Control (R/W) (BG Modes 0,1,2 only)
// 400000Eh - BG3CNT - BG3 Control (R/W) (BG Modes 0,2 only)
register_bitfields! {
    u16,
    pub BGXCnt [
        BGPriority OFFSET(0) NUMBITS(2),
        CharBaseBlock OFFSET(2) NUMBITS(2),
        Unused OFFSET(4) NUMBITS(2),
        Mosaic OFFSET(6) NUMBITS(1),
        PalleteType OFFSET(7) NUMBITS(1),
        ScreenBaseBlock OFFSET(8) NUMBITS(5),
        DispAreaOverFlow OFFSET(13) NUMBITS(1),
        ScreenSize OFFSET(14) NUMBITS(2),
    ],
}
