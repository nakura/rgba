use super::common::*;
use crate::core::bounds::*;
use crate::core::mem::*;
use core::marker::PhantomData;

pub trait VramWidth: TypeWidth {}
impl VramWidth for u16 {}
impl VramWidth for u32 {}

pub struct Vram<const START: usize, const SIZE: usize>
where
    If<{ SIZE <= VRAM_TOTAL_SIZE }>: True,
    If<{ (VRAM_START + SIZE) <= VRAM_END }>: True;

impl<const START: usize, const SIZE: usize> Vram<START, SIZE>
where
    If<{ SIZE <= VRAM_TOTAL_SIZE }>: True,
    If<{ (VRAM_START + SIZE) <= VRAM_END }>: True,
{
    pub const fn new<W: TypeWidth>() -> AddrBlk<W, RW, SIZE> {
        AddrBlk::new(START)
    }
}

pub type VramMode3 = Vram<VRAM_START, VRAM_MODE3_SIZE>;

pub trait PalRamWidth = VramWidth;

pub type BgPalRamBlk<T> = AddrBlk<T, RW, BG_PAL_RAM_SIZE>;
pub type ObjPalRamBlk<T> = AddrBlk<T, RW, OBJ_PAL_RAM_SIZE>;

pub struct PalRam<T>
where
    T: PalRamWidth,
{
    pub bg: BgPalRamBlk<T>,
    pub obj: ObjPalRamBlk<T>,
}

impl<T> PalRam<T>
where
    T: PalRamWidth,
{
    pub const fn new() -> Self {
        Self {
            bg: BgPalRamBlk::new(BG_PAL_RAM_START),
            obj: ObjPalRamBlk::new(OBJ_PAL_RAM_START),
        }
    }
}

// This is kinda fucked up, surely this can be simplified?
pub struct Block<T, const BLOCK_NUM: usize, const BLOCK_SIZE: usize>(PhantomData<T>);

impl<T, const BLOCK_NUM: usize, const BLOCK_SIZE: usize> Block<T, BLOCK_NUM, BLOCK_SIZE>
where
    T: VramWidth,
    If<
        {
            ((BLOCK_NUM < 4) & (BLOCK_SIZE == CHAR_BLOCK_SIZE))
                | ((BLOCK_NUM < 31) & (BLOCK_SIZE == SCREEN_BLOCK_SIZE))
        },
    >: True,
{
    pub const fn new() -> AddrBlk<T, RW, BLOCK_SIZE> {
        AddrBlk::new(VRAM_START + (BLOCK_NUM * BLOCK_SIZE))
    }
}

pub type CharAddrBlock<T> = AddrBlk<T, RW, CHAR_BLOCK_SIZE>;
pub type ScreenAddrBlock<T> = AddrBlk<T, RW, SCREEN_BLOCK_SIZE>;

pub type CharBlock<T, const BLOCK_NUM: usize> = Block<T, BLOCK_NUM, CHAR_BLOCK_SIZE>;
pub type ScreenBlock<T, const BLOCK_NUM: usize> = Block<T, BLOCK_NUM, SCREEN_BLOCK_SIZE>;
