#![feature(const_fn_trait_bound)]
#![feature(const_generics)]
#![feature(const_evaluatable_checked)]
#![feature(trait_alias)]
#![feature(const_panic)]
#![feature(lang_items)]
#![no_std]
use ::core::panic::PanicInfo;
//#[cfg(feature = "font")]
pub mod core;
pub mod font;
pub mod graphics;
#[macro_use]
pub mod util;
extern crate tock_registers;
extern crate typenum;

pub mod prelude {
    pub use tock_registers::interfaces::{ReadWriteable, Readable, Writeable};
}

use ::core::fmt::Write;
use font::font::setup_font;
use graphics::{lcd::*, mem::*, regs::*};
use prelude::*;

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    setup_font();

    const SCREEN_BLOCK_4: ScreenAddrBlock<u16> = ScreenBlock::<u16, 4>::new();

    let mut screen_writer = ScreenWriter::new(SCREEN_BLOCK_4);

    screen_writer.write_fmt(format_args!("{}", info)).unwrap();

    LCD.regs.bg0cnt.write(BGXCnt::ScreenBaseBlock.val(4));

    LCD.regs
        .dispcnt
        .write(DispCnt::BgMode::Mode0 + DispCnt::DisplayBG0::SET);

    loop {}
}
